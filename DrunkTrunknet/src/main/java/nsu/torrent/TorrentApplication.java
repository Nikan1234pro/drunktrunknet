package nsu.torrent;

import javafx.application.Application;
import javafx.stage.Stage;
import nsu.torrent.ui.base.windowManager.WindowManager;

public class TorrentApplication extends Application {
    public static final int WINDOW_DEFAULT_WIDTH = 1600;
    public static final int WINDOW_DEFAULT_HEIGHT = 900;

    private static Stage stage;

    public static Stage getStage() {
        return stage;
    }

    @Override
    public void start(Stage primaryStage) {
        stage = primaryStage;

        final String entranceForm = "/entrance/entrance.fxml";

        WindowManager manager = new WindowManager(primaryStage, WINDOW_DEFAULT_WIDTH, WINDOW_DEFAULT_HEIGHT);
        manager.showScene(manager.loadScene(entranceForm));
    }
    public static void main(String[] args) {
        launch(args);
    }
}
