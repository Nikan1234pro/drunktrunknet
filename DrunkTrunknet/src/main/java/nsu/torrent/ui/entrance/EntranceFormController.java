package nsu.torrent.ui.entrance;

import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBasicCloseTransition;
import com.jfoenix.transitions.hamburger.HamburgerSlideCloseTransition;
import javafx.animation.TranslateTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import nsu.torrent.TorrentApplication;
import nsu.torrent.ui.base.windowManager.WindowController;
import nsu.torrent.ui.base.windowManager.WindowManager;
import nsu.torrent.ui.entrance.pages.newDownload.NewDownloadController;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class EntranceFormController extends WindowController implements Initializable {
    private VBox pageKeeper;

    @FXML
    private Label logoLabel;

    @FXML
    private AnchorPane rootPane;

    @FXML
    private Button downloadButton;

    @FXML
    private JFXHamburger hamburger;


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setWindowManager(new WindowManager(rootPane));
        setupToolbar();
    }

    @FXML
    void onDownloadButtonClicked(MouseEvent event) {
        final String fxmlFile = "/entrance/pages/newDownload/newDownload.fxml";

        WindowManager manager = getWindowManager();
        NewDownloadController controller = manager.loadScene(fxmlFile, this);
        manager.showScene(controller);
    }

    private void setupToolbar() {
        final int toolbarWidth = 200;
        final String toolbarFile = "/entrance/toolbar.fxml";
        final double slideDuration = 300.0;

        pageKeeper = new VBox();
        pageKeeper.prefWidthProperty().bind(rootPane.widthProperty());
        pageKeeper.prefHeightProperty().bind(rootPane.heightProperty());
        rootPane.getChildren().add(pageKeeper);
        try {
            VBox toolbar = FXMLLoader.load(getClass().getResource(toolbarFile));
            toolbar.prefHeightProperty().bind(rootPane.heightProperty());
            toolbar.setPrefWidth(toolbarWidth);
            toolbar.setTranslateX(-toolbar.getPrefWidth());

            TranslateTransition toolbarTranslation =
                    new TranslateTransition(Duration.millis(slideDuration), toolbar);

            toolbarTranslation.setFromX(-toolbar.getPrefWidth());
            toolbarTranslation.setToX(0);
            toolbarTranslation.setRate(-1);

            HamburgerSlideCloseTransition buttonTranslation =
                    new HamburgerSlideCloseTransition(hamburger);
            buttonTranslation.setRate(-1);

            hamburger.setOnMouseClicked(e -> {
                toolbarTranslation.setRate(toolbarTranslation.getRate() * -1);
                buttonTranslation.setRate(buttonTranslation.getRate() * -1);

                toolbarTranslation.play();
                buttonTranslation.play();
            });
            rootPane.getChildren().add(toolbar);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
