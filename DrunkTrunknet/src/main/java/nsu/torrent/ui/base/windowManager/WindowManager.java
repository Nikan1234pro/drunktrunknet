package nsu.torrent.ui.base.windowManager;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.Stack;

public class WindowManager {
    private Stack<WindowController> controllers;
    private Pane pageKeeper;


    public WindowManager(Stage primaryStage, int width, int height) {
        pageKeeper = new Pane();
        controllers = new Stack<>();

        Scene scene = new Scene(pageKeeper, width, height);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public WindowManager(Pane root) {
        pageKeeper = root;
        controllers = new Stack<>();
    }


    public <C extends WindowController> C loadScene(String url) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(url));
            Node root = fxmlLoader.load();

            C controller = fxmlLoader.getController();
            controller.setView(root);

            return controller;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public <C extends WindowController> C loadScene(String url, WindowController parent) {
        C controller = loadScene(url);
        if (controller == null)
            return null;

        if (parent == null)
            return controller;

        controller.setParent(parent);
        controller.setWindowManager(parent.getWindowManager());
        return controller;
    }


    public void resetScenes(WindowController controller) {
        controllers.clear();
        showScene(controller);
    }

    public void showScene(WindowController controller) {
        show(controller);
        controllers.add(controller);
    }

    private void show(WindowController controller) {
        Pane page = (Pane) controller.getView();
        page.prefWidthProperty().bind(pageKeeper.widthProperty());
        page.prefHeightProperty().bind(pageKeeper.heightProperty());

        pageKeeper.getChildren().clear();
        pageKeeper.getChildren().add(page);
    }

    public void prevScene() {
        if (controllers.empty()) {
            System.err.println("Scenes stack are empty!");
            return;
        }
        controllers.pop();
        show(controllers.peek());
    }
}
