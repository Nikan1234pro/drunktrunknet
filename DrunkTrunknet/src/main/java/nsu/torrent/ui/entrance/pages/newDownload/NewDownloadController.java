package nsu.torrent.ui.entrance.pages.newDownload;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.FileChooser;
import nsu.torrent.TorrentApplication;
import nsu.torrent.ui.base.windowManager.WindowController;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class NewDownloadController extends WindowController implements Initializable {
    @FXML
    private Button fileSelectorButton;

    private final String filterName = "Torrent files (*.torrent)";
    private final String extension = "*.torrent";

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setupFileSelector();
    }

    private void setupFileSelector() {
        fileSelectorButton.setOnMouseClicked(e -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Resource File");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(filterName, extension));
            File file = fileChooser.showOpenDialog(TorrentApplication.getStage());
            if (file == null) {
                return;
            }
            handleSelectedFile(file);
        });
    }

    private void handleSelectedFile(File file) {
        System.out.println(file.getAbsolutePath());
    }
}
