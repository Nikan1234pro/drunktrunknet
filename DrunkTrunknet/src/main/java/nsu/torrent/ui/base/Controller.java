package nsu.torrent.ui.base;

import javafx.scene.Node;

public interface Controller {
    Node getView();
    void setView(Node view);
}
