package nsu.torrent.ui.base.windowManager;


import nsu.torrent.ui.base.BaseController;

public class WindowController extends BaseController {
    WindowManager manager = null;
    private WindowController parent;

    public void setWindowManager(WindowManager manager) {
        this.manager = manager;
    }

    public WindowManager getWindowManager() {
        return manager;
    }

    public WindowController getParent() {
        return parent;
    }

    public void setParent(WindowController parent) {
        this.parent = parent;
    }
}
